# Cordova Plugin Local Notification

## Instalação 

1. Adicionar o plugin no projeto

```javascript
cordova plugin add https://EmersonJS@bitbucket.org/EmersonJS/local-notification.git
```

2. Remover o plugin do projeto

```javascript
cordova plugin remove local-notification
```

## Como usar

- Para pegar o callback das notificações é necessário:

1. adicionar o listener da notificações

```javascript

window["MyCordovaPlugin"].on('notification', function(response) {
    console.log(response);
});

```

2. adicionar e chamar o método Ready, necessário para avisar o cordova que o load da página já finalizou.

```javascript

window["MyCordovaPlugin"].ready(function(response) {
    console.log('localnotification ready');
});

```

- Para agendar uma notificação é necessário chamar o método

```javascript

this.platform.ready().then(() => {
    window["MyCordovaPlugin"].schedule({key: 1, 
                                        data: {idMedicamento: 1},//obrigatorio informar algo!!!
                                        title: 'lembrete de medicamento', intervalDay: 1, 
                                        startDate: '2018-01-28', endDate: '2018-01-28', 
                                        hours: [''+this.time+'']}, (response) => {
        if(response) {
            alert('Evento agendado com sucesso');
        }
        else {
            alert('Não foi possível realizar o agendamento');
        }
    });
});

```


- Exemplos de parametro

```javascript

{title: '', mensagem: '', intervalDay: 2 (para 2-2 dias), hours: [], startDate: 'YYYY-MM-DD', endDate: 'YYYY-MM-DD', key: 1212, data: {}}
{title: '', mensagem: '', intervalDay: 1, hours: ['08:00'], startDate: 'YYYY-MM-DD', key: 1212, data: {}}
{title: '', mensagem: '', intervalDay: 1, hours: ['08:00','23:00'], startDate: 'YYYY-MM-DD', key: 1212, data: {}}
{title: '', mensagem: '', hours: [08:00,23:00], startDate: 'YYYY-MM-DD', key: 1212, data: {}}
    
```