package com.example;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emersonjose on 27/01/18.
 */

@SuppressWarnings({"Convert2Diamond", "Convert2Lambda"})
public class AlarmConfig {

    public  AlarmConfig() {
        this.setIntervalDay(1);
        this.alarmKeys = new ArrayList();
    }

    private String title;
    private String description;
    private String startDate;
    private String endDate;
    private String data;
    private List<String> hours;
    private Integer intervalDay;
    private List<Integer> alarmKeys;
    private int key;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<String> getHours() {
        return hours;
    }

    public void setHours(List<String> hours) {
        this.hours = hours;
    }

    public Integer getIntervalDay() {
        return intervalDay;
    }

    public void setIntervalDay(Integer intervalDay) {
        this.intervalDay = intervalDay;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, AlarmConfig.class);
    }

    public  static  AlarmConfig parseString(String json) {
        return new Gson().fromJson(json, AlarmConfig.class);
    }

    public List<Integer> getAlarmKey() {
        return alarmKeys;
    }

    public void addAlarmKey(Integer alarmKey) {

        this.alarmKeys.add(alarmKey);
    }

    public void clearAlarmKeys() {
        this.alarmKeys = new ArrayList();
    }
}
