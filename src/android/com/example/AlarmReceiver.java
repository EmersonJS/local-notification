package com.example;

/**
 * Created by emersonjose on 27/01/18.
 */

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.americasmed.clientes.MainActivity;
import br.com.americasmed.clientes.R;

@SuppressWarnings({"Convert2Diamond", "Convert2Lambda"})
public class AlarmReceiver extends BroadcastReceiver {
    public static String ALARM_ID = "alarm-id";
    public static String NOTIFICATION = "notification";

    @Override
    public void onReceive(Context context, Intent intent) {
        int idAlarmNotification = intent.getIntExtra(ALARM_ID, 0);

        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = intent.getParcelableExtra(NOTIFICATION);
        notificationManager.notify(idAlarmNotification, notification);
    }

    public void loadAlarm(Context context)
    {
       List<AlarmConfig> configs = AlarmStorage.instance(context).get();

        for (AlarmConfig config : configs) {
            this.setAlarm(context, config, false);
        }
    }

    public void setAlarm(Context context, AlarmConfig config, boolean isPersist)
    {
        boolean isRepeat = config.getEndDate() == null || config.getEndDate().isEmpty();
        Calendar today = Calendar.getInstance();

        Calendar endDateTrigger = Calendar.getInstance();

        ArrayList<Integer> pStartDate = getDate(config.getStartDate());
        Calendar todayTrigger = Calendar.getInstance();
        todayTrigger.set(pStartDate.get(0), (pStartDate.get(1) -1), pStartDate.get(2));

        Calendar dataReference = Calendar.getInstance();
        dataReference.set(pStartDate.get(0), (pStartDate.get(1) -1), pStartDate.get(2));

        boolean nextDay = false;
        if(!isRepeat) {
            ArrayList<Integer> pEndDate = getDate(config.getEndDate());
            endDateTrigger.set(pEndDate.get(0), (pEndDate.get(1) -1), pEndDate.get(2));
        }

        config.clearAlarmKeys();

        SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");

        while (todayTrigger.getTimeInMillis() <= endDateTrigger.getTimeInMillis()) {

            for(String hour: config.getHours()) {

                ArrayList<Integer> pHour = getHour(hour);
                todayTrigger.set(Calendar.HOUR_OF_DAY, pHour.get(0));
                todayTrigger.set(Calendar.MINUTE, pHour.get(1));
                todayTrigger.set(Calendar.SECOND, 0);
                todayTrigger.set(Calendar.MILLISECOND, 0);

                dataReference.set(Calendar.HOUR_OF_DAY, pHour.get(0));
                dataReference.set(Calendar.MINUTE, pHour.get(1));
                dataReference.set(Calendar.SECOND, 0);
                dataReference.set(Calendar.MILLISECOND, 0);

                if(!isRepeat) {
                    endDateTrigger.set(Calendar.HOUR_OF_DAY, pHour.get(0));
                    endDateTrigger.set(Calendar.MINUTE, pHour.get(1));
                    endDateTrigger.set(Calendar.SECOND, 0);
                    endDateTrigger.set(Calendar.MILLISECOND, 0);
                }

                if((today.getTimeInMillis() >= todayTrigger.getTimeInMillis() && isRepeat) ||
                        (isRepeat && today.getTimeInMillis() >= todayTrigger.getTimeInMillis() && endDateTrigger.getTimeInMillis() > todayTrigger.getTimeInMillis())) {

                    int addDay = + today.getTimeInMillis() >= todayTrigger.getTimeInMillis() ? 1 : 0;
                    todayTrigger.add(Calendar.DAY_OF_MONTH, today.getTime().getDay() + addDay);

                    nextDay = true;
                }

                int requestCode = config.getKey() + ((int) (todayTrigger.getTimeInMillis() % Integer.MAX_VALUE));

                //create notification trigger click
                Intent intentNotification = new Intent(context, NotificationActivity.class);
                intentNotification.putExtra(NotificationActivity.NOTIFICATION_ID, config.getData());
                PendingIntent pIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), intentNotification, 0);

                //setting up notification
                AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
                Intent notificationIntent = new Intent(context, AlarmReceiver.class);
                notificationIntent.putExtra(AlarmReceiver.ALARM_ID, requestCode);
                notificationIntent.putExtra(AlarmReceiver.NOTIFICATION, getNotification(context, pIntent, config.getTitle(), config.getDescription()));

                PendingIntent pi = PendingIntent.getBroadcast(context, requestCode, notificationIntent, 0);

                if(isRepeat) {
                    am.setRepeating(AlarmManager.RTC_WAKEUP, todayTrigger.getTimeInMillis(), AlarmManager.INTERVAL_DAY * config.getIntervalDay(), pi);
                    Log.d("**schedule repeat **", df.format(todayTrigger.getTime()));

                    if(nextDay) {
                        todayTrigger.add(Calendar.DAY_OF_MONTH, pStartDate.get(2));
                        nextDay = false;
                    }

                    config.addAlarmKey(requestCode);
                }
                else if((todayTrigger.getTimeInMillis() >= today.getTimeInMillis()) || (today.getTimeInMillis() >= todayTrigger.getTimeInMillis() && isRepeat)) {
                    am.set(AlarmManager.RTC_WAKEUP, todayTrigger.getTimeInMillis(), pi);
                    Log.d("**schedule endate **", df.format(todayTrigger.getTime()));

                    config.addAlarmKey(requestCode);
                }

                Log.d("Register NotificationID", String.valueOf(requestCode));
            }

            today.set(Calendar.HOUR_OF_DAY, 0);
            today.set(Calendar.MINUTE, 0);
            today.set(Calendar.MILLISECOND, 0);
            today.add(Calendar.DAY_OF_MONTH, 1);

            if(isRepeat) {
                todayTrigger.add(Calendar.DAY_OF_MONTH, endDateTrigger.getTime().getDay() + 1);

                Log.d("**start **", df.format(todayTrigger.getTime()));
                Log.d("**end **", df.format(endDateTrigger.getTime()));
            }
            else {
                todayTrigger.add(Calendar.DAY_OF_MONTH, 1);
            }
        }

        if(isPersist) {
            AlarmStorage.instance(context).save(config);
        }
    }

    public void cancelAlarm(Context context, AlarmConfig config)
    {
        for (Integer key : config.getAlarmKey()) {

            Intent intent = new Intent(context, AlarmReceiver.class);
            PendingIntent sender = PendingIntent.getBroadcast(context, key, intent, 0);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(sender);

            Log.d("Cancel NotificationID", String.valueOf(key));
        }
    }

    private Notification getNotification(Context context, PendingIntent pedingIntent, String title, String description) {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentTitle(title);
        builder.setContentText(description);
        builder.setSmallIcon(R.mipmap.icon);
        builder.setSound(alarmSound);
        builder.setAutoCancel(true);
        builder.setContentIntent(pedingIntent);
        return builder.build();
    }

    private ArrayList<Integer> getDate(String date) {
        String[] dateArray = date.split("-");

        ArrayList<Integer> response = new ArrayList();
        response.add(Integer.valueOf(dateArray[0]));
        response.add(Integer.valueOf(dateArray[1]));
        response.add(Integer.valueOf(dateArray[2]));

        return response;
    }

    private ArrayList<Integer> getHour(String hour) {
        String[] hourArray = hour.split(":");

        ArrayList<Integer> response = new ArrayList();
        response.add(Integer.valueOf(hourArray[0]));
        response.add(Integer.valueOf(hourArray[1]));

        return response;
    }
}