package com.example;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by emersonjose on 27/01/18.
 */

public class AlarmStorage {
    private static final String TAG = "AlarmStorage";

    private SharedPreferences mPreferences;

    private SharedPreferences.Editor mEditor;

    private static AlarmStorage mSPHelper;

    Type listOfObject = new TypeToken<List<AlarmConfig>>(){}.getType();

    Gson gson = new Gson();

    public static AlarmStorage instance(Context context) {
        if (mSPHelper == null)
            mSPHelper = new AlarmStorage(context);

        return mSPHelper;
    }

    private AlarmStorage(Context context) {
        mPreferences = context.getSharedPreferences(TAG, Context.MODE_APPEND);
    }

    public boolean save(AlarmConfig value) {
        mEditor = mPreferences.edit();

        List<AlarmConfig> alarmArray = this.get();
        alarmArray.add(value);

        String json = gson.toJson(alarmArray, listOfObject);

        mEditor.putString("CURRENT_ALARM", json);

        return mEditor.commit();
    }

    public List<AlarmConfig> get() {

        String json = mPreferences.getString("CURRENT_ALARM", "");

        if(json.isEmpty()) {
            return new ArrayList();
        }

        return gson.fromJson(json, listOfObject);
    }

    public Boolean delete(String key) {
        mEditor = mPreferences.edit();

        List<AlarmConfig> items = get();

        for (Iterator<AlarmConfig> iter = items.listIterator(); iter.hasNext(); ) {
            AlarmConfig item = iter.next();

            if(item.getData() == key) {
                iter.remove();
            }
        }

        String json = gson.toJson(items, listOfObject);

        mEditor.putString("CURRENT_ALARM", json);

        return mEditor.commit();
    }

    public AlarmConfig getByKey(String key) {

        for(AlarmConfig config : get()) {
            if(config.getData() == key) {
                return config;
            }
        }

        return null;
    }

    public boolean clear() {
        mEditor = mPreferences.edit();
        mEditor.remove("CURRENT_USER");
        return mEditor.commit();
    }
}
