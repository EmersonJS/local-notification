/**
 */
package com.example;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import android.util.Log;
import java.util.Arrays;

public class MyCordovaPlugin extends CordovaPlugin {
  private static final String TAG = "MyCordovaPlugin";

  AlarmReceiver alarm = new AlarmReceiver();

  public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    super.initialize(cordova, webView);

    Log.d(TAG, "Initializing MyCordovaPlugin");
  }

  public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
    if(action.equals("registerNotification")) {
      AlarmConfig alarmModel = AlarmConfig.parseString(args.get(0).toString());
      alarm.setAlarm(cordova.getActivity(), alarmModel, true);

      final PluginResult result = new PluginResult(PluginResult.Status.OK, "{\"success\": true}");
      callbackContext.sendPluginResult(result);

    } else if(action.equals("clear")) {

      AlarmConfig alarmModel = AlarmConfig.parseString(args.get(0).toString());
      alarmModel = AlarmStorage.instance(cordova.getActivity()).getByKey(alarmModel.getData());

      alarm.cancelAlarm(cordova.getActivity(), alarmModel);
      AlarmStorage.instance(cordova.getActivity()).delete(alarmModel.getData());

      final PluginResult result = new PluginResult(PluginResult.Status.OK, "{\"success\": true}");
      callbackContext.sendPluginResult(result);

    } else if(action.equals("ready")) {
      final PluginResult result = new PluginResult(PluginResult.Status.OK, "{\"success\": true}");
      callbackContext.sendPluginResult(result);
    }

    return true;
  }

}
