package com.example;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import org.apache.cordova.CordovaActivity;

import br.com.americasmed.clientes.MainActivity;

public class NotificationActivity extends CordovaActivity {
    public static String NOTIFICATION_ID = "notification-id";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        super.onCreate(savedInstanceState);

        // enable Cordova apps to be started in the background
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getBoolean("cdvStartInBackground", false)) {
            moveTaskToBack(true);
        }

        loadUrl(launchUrl);

        final String data = extras.getString(NOTIFICATION_ID);
        Log.d("**NotificationID", data);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                NotificationActivity.this.appView.sendJavascript("javascript:window['MyCordovaPlugin'].fireEvent('notification', '"+data+"')");
            }
        }, 5000);
    }
}
