#import <Cordova/CDVPlugin.h>

@import UserNotifications;

@interface MyCordovaPlugin : CDVPlugin<UNUserNotificationCenterDelegate> {
}

@property (strong, nonatomic) NSString* userInfo;
@property (strong, nonatomic) UNUserNotificationCenter* center;
@property (readwrite, assign) BOOL isActive;

- (void) scheduleEvent: (NSString *) key  allowRepeat: (BOOL *) isRepeat  setDateTrigger: (NSDateComponents *)dateTrigger setContent: (UNMutableNotificationContent *) content;
- (void)registerNotification:(CDVInvokedUrlCommand *)command;
- (void)ready:(CDVInvokedUrlCommand *)command;
- (void) onFireEvent;

@end
