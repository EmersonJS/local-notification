#import "MyCordovaPlugin.h"
#import <Cordova/CDVAvailability.h>
#import <UserNotifications/UserNotifications.h>

@implementation MyCordovaPlugin

@synthesize userInfo, isActive;

- (void)registerNotification:(CDVInvokedUrlCommand *)command {
    NSDictionary *options = command.arguments[0];
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"yyyy-mm-dd";
    
    NSDateFormatter* formatterTime = [[NSDateFormatter alloc]init];
    formatterTime.dateFormat = @"HH:MM";
    
    NSDate *todayDate = [NSDate date];
    NSDateComponents *todayComponet = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute fromDate:todayDate];
    
    BOOL isRepeat = YES;
    int intervalOfDay = 1;
    
    UNMutableNotificationContent* content = [[UNMutableNotificationContent alloc] init];
    content.sound = [UNNotificationSound defaultSound];
    
    if([options valueForKey:@"title"] != nil) {
        content.title = [options valueForKey:@"title"];
    }
    
    if([options valueForKey:@"mensagem"] != nil) {
        content.body = [options valueForKey:@"mensagem"];
    }
    
    if([options valueForKey:@"data"] != nil) {
        content.userInfo = @{@"data": [options valueForKey:@"data"]};
    }
    
    if([options valueForKey:@"intervalDay"] != nil) {
        intervalOfDay = [[options valueForKey:@"intervalDay"] intValue];
    }
    
    NSDate* startDate;
    
    if([options valueForKey:@"startDate"] != nil) {
        startDate = [formatter dateFromString:[options valueForKey:@"startDate"]];
    }
    
    //set end date with today date if repeat is allow
    NSDate* endDate = startDate;
    
    if([options valueForKey:@"endDate"] != nil) {
        endDate = [formatter dateFromString:[options valueForKey:@"endDate"]];
        isRepeat = NO;
    }
    else {
        endDate = [startDate dateByAddingTimeInterval: 60*60*24*1];
    }
    
    while ([startDate compare: endDate] != NSOrderedDescending) {
        NSDateComponents *startTrigger = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute fromDate:startDate];
        
        NSArray *hours = [options objectForKey:@"hours"];
        
        for (NSString *hour in hours) {
            NSArray *params = [hour componentsSeparatedByString: @":"];
            startTrigger.hour = [params[0] intValue];
            startTrigger.minute = [params[1] intValue];
            
            NSString *key = [NSString stringWithFormat:@"%@-%@", [options objectForKey:@"key"], [self uuid]];
        
            if([endDate compare: startDate] == NSOrderedDescending) {
                NSLog(@"Future schedule: %@ - %@ %@", key, [formatter stringFromDate:startDate], hour);
                [self scheduleEvent: key allowRepeat: &isRepeat setDateTrigger:startTrigger setContent:content];
            } else {
                if(!([todayComponet hour] > [startTrigger hour] && ([todayComponet minute] > [startTrigger minute]))) {
                    NSLog(@"Start today schedule: %@ - %@ %@", key, [formatter stringFromDate:startDate], hour);
                    [self scheduleEvent: key allowRepeat: &isRepeat setDateTrigger:startTrigger setContent:content];
                }
            }
        }
        
        startDate = [startDate dateByAddingTimeInterval: 60*60*24*intervalOfDay];
    }
    
    [self sendCallback: command];
}

- (void) scheduleEvent: (NSString *) key  allowRepeat: (BOOL *) isRepeat  setDateTrigger: (NSDateComponents *)dateTrigger setContent: (UNMutableNotificationContent *) content
{
    UNCalendarNotificationTrigger* trigger = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:dateTrigger repeats:isRepeat];
    
    
    UNNotificationRequest* request = [UNNotificationRequest
                                      requestWithIdentifier:key content:content trigger:trigger];
    
    UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"%@", error.localizedDescription);
        }
    }];
}

- (void)pluginInitialize {
    _center = [UNUserNotificationCenter currentNotificationCenter];
    _center.delegate = self;
    
    [self monitorAppStateChanges];
    
    [_center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error)
     {
         if( !error ) {
             NSLog( @"Push registration success." );
         } else {
             NSLog( @"Push registration FAILED" );
         }
     }];
}

-(void) ready:(CDVInvokedUrlCommand *)command
{
    if(userInfo != nil) {
        [self onFireEvent];
    }
    
    [self sendCallback: command];
}

- (void) clear:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        NSDictionary *options = command.arguments[0];
        
        [[UNUserNotificationCenter currentNotificationCenter] getPendingNotificationRequestsWithCompletionHandler:^(NSArray<UNNotificationRequest *> * _Nonnull requests) {
            NSLog(@"count %lu",(unsigned long)requests.count);
            
            for (NSInteger x = 0; x < requests.count; x++) {
                UNNotificationRequest *pendingRequest = [requests objectAtIndex:x];
                NSLog(@"%@", pendingRequest.identifier);
                NSString *key = [pendingRequest.identifier componentsSeparatedByString: @"-"][0];
                
                if ([key isEqualToString: [[options objectForKey:@"key"] stringValue]]) {
                    [[UNUserNotificationCenter currentNotificationCenter]removePendingNotificationRequestsWithIdentifiers:@[pendingRequest.identifier]];
                }
            }
            
            //[self sendCallback: command];
            [self onFireEvent];
        }];
    }];
}

- (void) userNotificationCenter:(UNUserNotificationCenter *)center
        willPresentNotification:(UNNotification *)notification
          withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler
{
    userInfo = [notification.request.content.userInfo objectForKey:@"data"];
    
    if(isActive) {
        [self onFireEvent];
    }
    
    completionHandler(UNNotificationPresentationOptionNone);
}

- (void) userNotificationCenter:(UNUserNotificationCenter *)center
 didReceiveNotificationResponse:(UNNotificationResponse *)response
          withCompletionHandler:(void (^)(void))completionHandler
{
    userInfo = [response.notification.request.content.userInfo objectForKey:@"data"];
    
    [self onFireEvent];
    completionHandler();
}

- (void) monitorAppStateChanges
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [center addObserverForName:UIApplicationDidBecomeActiveNotification
                        object:NULL queue:[NSOperationQueue mainQueue]
                    usingBlock:^(NSNotification *e) { isActive = YES; }];
    
    [center addObserverForName:UIApplicationDidEnterBackgroundNotification
                        object:NULL queue:[NSOperationQueue mainQueue]
                    usingBlock:^(NSNotification *e) { isActive = NO; }];
}

- (void) onFireEvent
{
    [self.commandDelegate evalJs: [NSString stringWithFormat:@"window['MyCordovaPlugin'].fireEvent('notification', '%@')", userInfo]];
}

- (void) sendCallback: (CDVInvokedUrlCommand *)command
{
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"{\"success\": true}"];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (NSString*) encodeToJSON:(NSDictionary*)userInfo
{
    NSString* json;
    NSData* data;
    NSMutableDictionary* obj = [userInfo mutableCopy];
    
    [obj removeObjectForKey:@"updatedAt"];
    
    if (obj == NULL || obj.count == 0)
        return json;
    
    data = [NSJSONSerialization dataWithJSONObject:obj
                                           options:NSJSONWritingPrettyPrinted
                                             error:NULL];
    
    json = [[NSString alloc] initWithData:data
                                 encoding:NSUTF8StringEncoding];
    
    return [json stringByReplacingOccurrencesOfString:@"\n"
                                           withString:@""];
}

- (NSString *)uuid
{
    CFUUIDRef uuidRef = CFUUIDCreate(NULL);
    CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
    CFRelease(uuidRef);
    return (__bridge_transfer NSString *)uuidStringRef;
}

@end
