var exec = require('cordova/exec');
var PLUGIN_NAME = 'MyCordovaPlugin';

var LocalNotification = {
  events: [],

  fireEvent: function (event) {
    var args     = Array.apply(null, arguments).slice(1),
        listener = this.events[event];

    if (!listener)
        return;

    if (args[0] && typeof args[0].data === 'string') {
        args[0].data = JSON.parse(args[0].data);
    }

    for (var i = 0; i < listener.length; i++) {
        var fn    = listener[i][0],
            scope = listener[i][1];

        fn.apply(scope, args);
    }
  },
  on: function (event, callback, scope) {

    if (typeof callback !== "function")
        return;

    if (!this.events[event]) {
        this.events[event] = [];
    }

    var item = [callback, scope || window];

    this.events[event].push(item);
  },
  schedule: function(data, cb) {
    exec(cb, null, PLUGIN_NAME, 'registerNotification', [data]);
  },
  ready: function(cb) {
    exec(cb, null, PLUGIN_NAME, 'ready', []);
  },
  clear: function(data, cb) {
    exec(cb, null, PLUGIN_NAME, 'clear', [data]);
  }
};

module.exports = LocalNotification;